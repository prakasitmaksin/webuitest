import { Routes } from '@angular/router';
import { CategoryListComponent } from './features/category/category-list/category-list.component';
import { CategoryAddComponent } from './features/category/category-add/category-add.component';

export const routes: Routes = [
    {
        path: 'home/categories',
        component: CategoryListComponent
    },
    {
        path: 'home/categories/add',
        component: CategoryAddComponent
    }
];
