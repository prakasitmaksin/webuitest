import { Component,OnInit  } from '@angular/core'; 
import { FormGroup, FormControl, Validators , FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClient } from '@angular/common/http';  


@Component({
  selector: 'app-category-add',
  standalone: true,
  imports: [FormsModule,ReactiveFormsModule],
  templateUrl: './category-add.component.html',
  styleUrl: './category-add.component.scss'
})
export class CategoryAddComponent implements OnInit {

  formGroup!: FormGroup;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      name: new FormControl('', Validators.required),
      image: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    console.log('00');
    if (this.formGroup.valid) {
      const formData = new FormData();
      formData.append('name', this.formGroup.get('name')?.value);
      formData.append('image', this.formGroup.get('image')?.value);

      this.http.post('https://localhost:7175/api/BlogPost', formData).subscribe(() => {
      console.log('1');
      console.log('Data uploaded successfully!');
      }, (error) => {
        console.log(formData);
        console.log('Error uploading data:', error);
      });
    } else {
      console.log('03');
      console.log('Data lose!');
    }
  }
}