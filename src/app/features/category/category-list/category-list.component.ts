import { Component,OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-category-list',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './category-list.component.html',
  styleUrl: './category-list.component.scss'
})
export class CategoryListComponent implements OnInit {
  
  items: any[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.http.get('https://localhost:7175/api/BlogPost').subscribe((data: any) => {
      this.items = data;
      console.log(data);
    });
  }
}
